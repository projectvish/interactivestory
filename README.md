# README #

**Interactive Story** app is an android app for new android developers to understand the working of Activities in an app.

### What is this repository for? ###

This repo is to store an android application developed as a Demo to understand the working of Activities in an app.

### How do I get set up? ###

Open Android Studio

Import the project into Android Studio. [File->New->Import Project]

Compile and run.


* Configuration- Android Studio IDE, Genymotion (Simulator)